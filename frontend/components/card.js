import React from "react";
import Link from "next/link";
import NextImage from "./image";

const Card = ({ article }) => {
  return (
      <a className="uk-link-reset">
        <div class="uk-card uk-card-default uk-grid-collapse uk-margin" uk-grid>
          <div class="uk-card-media-left uk-cover-container">
              <NextImage image={article.attributes.image} />
          </div>
          <div>
          <div className="uk-card uk-card-body">
            <p id="category" className="uk-text-uppercase">
              {article.attributes.category?.data.attributes.name}
            </p>
            <p id="title" className="uk-text-large">
              {article.attributes.title}
            </p>
            <p className="uk-text-muted">
              {article.attributes.description}
            </p>
            <p className="uk-text-secondary">
              {article.attributes.content}
            </p>
          </div>
          </div>
      </div>
      </a>
  );
};

export default Card;