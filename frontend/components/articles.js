import React from "react";
import Card from "./card";

const Articles = ({ articles }) => {
  return (
    <div>
      <div className="uk-child-width-1-1@s" data-uk-grid="true">
        <div>
          <div className="uk-child-width-1-4@m uk-grid-match" data-uk-grid>
            {articles.map((article, i) => {
              return (
                <Card
                  article={article}
                  key={`article__left__${article.attributes.slug}`}
                />
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Articles;